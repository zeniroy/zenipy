
# -*- coding: utf-8 -*-
import itertools

def idxGenFor2Nested(nested):
    return list(itertools.chain.from_iterable((((i, j) for j in range(len(nested[i]))) for i in range(len(nested)))))

